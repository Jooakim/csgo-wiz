import pandas as pd
import numpy as np
import h5py
import math
from time import time

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

from keras.models import Sequential
from keras.layers import Dense, CuDNNLSTM, Dropout
from keras.optimizers import Adam
import keras


def load_data():
    with h5py.File('data/processed/data.h5', 'r') as hf:
        x_data = hf['x_data'][:]
        y_data = hf['y_data'][:]
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data)

    return x_train, x_test, y_train, y_test

    

def train_lstm(train_x, train_y):
    adam = Adam(lr=0.00001)
    model = Sequential()
    model.add(CuDNNLSTM(64, input_shape=(train_x.shape[1], train_x.shape[2]), 
        return_sequences=True))
    model.add(Dropout(0.5))
    model.add(CuDNNLSTM(32, return_sequences=False))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])

    tbCallBack = keras.callbacks.TensorBoard(log_dir='./logs/{}'.format(time()))

    model.fit(x=train_x, y=train_y, batch_size=1, epochs=200, validation_split=0.2,
            callbacks=[tbCallBack], verbose=1)

    return model

def train_random_forest(train_x, train_y):
    clf = RandomForestClassifier(n_jobs=-1, n_estimators=200)

    train_x = train_x.reshape((train_x.shape[0], train_x.shape[1]*train_x.shape[2]))
    clf.fit(train_x, train_y)

    return clf


def main():
    train_x, test_x, train_y, test_y = load_data()

    # Random forest
    random_forest_model = train_random_forest(train_x.copy(), train_y.copy())
    tmp_test_x = test_x.reshape((test_x.shape[0], test_x.shape[1]*test_x.shape[2]))
    rf_acc = accuracy_score(random_forest_model.predict(tmp_test_x), test_y)


    lstm_model = train_lstm(train_x, train_y)
    prediction = lstm_model.predict(test_x)
    prediction = np.round(prediction)
    lstm_acc = accuracy_score(prediction, test_y)


    print("Random Forest accuracy:", rf_acc)
    print("LSTM accuracy:", lstm_acc)
if __name__ == "__main__":
    main()
