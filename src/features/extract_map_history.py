import pandas as pd
import numpy as np

from src.utils.progress import print_progress_bar

def convert_to_bo1(data):
    games = [0, 1, 2, 3, 4]
    player_features = ['player', 'kast', 'kd', 'pm', 'adr', 'rating']
    map_features = ['map_name', 'win', 'winning_stats', 'losing_stats', 'started_as']
    game_features = ['date', 'team', 'opponent']

    all_games = list()
    for index, row in data.iterrows():
        print_progress_bar(index, len(data)-1)
        for game in games:
            game_stats = list()
            game_stats.extend(row[game_features])
            # Extract map features
            for feature in map_features:
                game_stats.append(row[feature+str(game)])

            # Extract specific player features
            for player in range(5):
                for feature in player_features:
                    game_stats.append(row[feature+str(game*10+player)])
            all_games.append(game_stats)

    feature_names = list(game_features)
    feature_names.extend(map_features)
    for i in range(5):
        feature_names.extend([feature+str(i) for feature in player_features])
    new_data = pd.DataFrame(all_games, columns=feature_names)

    return new_data

def fix_kd_features(data):
    f = ['kd0', 'kd1', 'kd2', 'kd3', 'kd4']
    for i, k in enumerate(f):
        a = data[k].values
        # Split values '14-12' into [14, 12] or nan to [nan, nan]
        a = np.array([s.split('-') if type(s) is str else [s, s] for s in a], dtype=np.float32)

        data['kills'+str(i)] = a[:, 0]
        data['deaths'+str(i)] = a[:, 1]
        data.drop(columns=k, inplace=True)
    return data

def fix_win(data):
    data.win = data.win.astype(int)
    return data


def main():
    data = pd.read_csv('data/interim/history_extracted_data.csv', low_memory=False)
    data = convert_to_bo1(data)
    data.dropna(inplace=True)
    data = fix_kd_features(data)
    data = fix_win(data)
    data.to_csv('data/interim/individual_map_data.csv', index=False)

    print(data.describe())
if __name__ == "__main__":
    main()
