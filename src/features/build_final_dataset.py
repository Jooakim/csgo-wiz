import pandas as pd
import numpy as np
from dateutil import parser
import h5py

from src.utils.progress import print_progress_bar


def find_games_between_teams(date, n_games, team1, team2, data):
    games = data.loc[(data['team'] == team1) & (data['opponent'] == team2)]
    games = games.loc[games['date'] < date]

    team1_frames = games.iloc[-n_games:]

    games = data.loc[(data['team'] == team2) & (data['opponent'] == team1)]
    games = games.loc[games['date'] < date]

    team2_frames = games.iloc[-n_games:]

    # Drop duplicate columns
    team2_frames.drop(columns=[
        'winning_stats', 'losing_stats', 'map_name', 'team', 'opponent'],
        inplace=True)


    return [team1_frames, team2_frames]


def find_last_games(date, n_games, team, data):
    team_games = data.loc[data['team'] == team]
    # TODO Handle when there is multiple games on the same
    # date and we want to preidict other than the first
    team_games = team_games.loc[team_games['date'] < date]
    return team_games.iloc[-n_games:]


def clean_data(data, match_date):
    dates = data['date'].values
    match_date = parser.parse(match_date)
    days_since_game = [(match_date - parser.parse(s)).days for s in dates]
    data['days_since_game'] = days_since_game
    # TODO Handle features containing text, such as map and players
    data.drop(columns=[
        'date', 'team', 'opponent', 'player0', 'player1',
        'player2', 'player3', 'player4', 'map_name', 'started_as'], inplace=True,
        errors='ignore')

    return data


def player_consisteny(data):
    """ Returns wheter the data consists of the same players in all games"""
    return len(data[
        ['player0', 'player1', 'player2', 'player3', 'player4']].drop_duplicates()) == 1


def load_data():
    data = pd.read_csv('data/interim/individual_map_data.csv')
    history_length = 5

    sequences = list()
    target = list()
    for index, row in data.iterrows():
        print_progress_bar(index, len(data) - 1)
        last_games_team1 = find_last_games(row['date'], history_length, row['team'], data)
        last_games_team2 = find_last_games(row['date'], history_length, row['opponent'], data)
        last_games_involving_both = find_games_between_teams(
            row['date'], history_length, row['team'], row['opponent'], data)

        if (len(last_games_involving_both[0]) != history_length or
                len(last_games_involving_both[1]) != history_length or
                len(last_games_team1) != history_length or
                len(last_games_team2) != history_length):
            continue

        if (not player_consisteny(last_games_team1) or
                not player_consisteny(last_games_team2) or
                not player_consisteny(last_games_involving_both[0]) or
                not player_consisteny(last_games_involving_both[1])):
            continue



        # Clean the data up, dropping irrelevant features
        last_games_team1 = clean_data(last_games_team1, row['date'])
        last_games_team2 = clean_data(last_games_team2, row['date'])
        last_games_involving_both[0] = clean_data(last_games_involving_both[0], row['date'])
        last_games_involving_both[1] = clean_data(last_games_involving_both[1], row['date'])

        feature_history = np.hstack([
            last_games_involving_both[0].values,
            last_games_involving_both[1].values,
            last_games_team1.values,
            last_games_team2.values])
        sequences.append(feature_history)
        target.append(row['win'])

    sequences = np.array(sequences)
    target = np.array(target)
    print(target)

    return sequences, target


def main():
    x_data, y_data = load_data()

    with h5py.File('data/processed/data.h5', 'w') as hf:
        hf.create_dataset('x_data', data=x_data)
        hf.create_dataset('y_data', data=y_data)


if __name__ == "__main__":
    main()
