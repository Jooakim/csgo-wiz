import pandas as pd
import numpy as np
import json
import sys

from src.utils.progress import print_progress_bar

def convert_raw_history_to_feature_history(all_data, switch_start=False):
    dataframe = pd.DataFrame()
    history = list()
    for index, data in all_data.iterrows():
        features = ['date', 'team', 'opponent']
        players = sorted(data['team1_players'])
        team = data['team1']
        game_vec = list()
        game_vec.append(data['date'])
        game_vec.append(team)
        game_vec.append(data['team2'])
        for game_index, game in enumerate(data['map_results']):
            features.extend(['map_name'+str(game_index), 'started_as'+str(game_index),
                'win'+str(game_index), 'winning_stats'+str(game_index), 
                'losing_stats'+str(game_index)])
            game_vec.append(game['map_name'])
            if switch_start:
                game_vec.append('ct' if game['team1_started_as'] == 't' else 't')
            else: 
                game_vec.append(game['team1_started_as'])
            game_vec.append(team == game['winner'])
            game_vec.append(game['winning_stats'])
            game_vec.append(game['losing_stats'])
            for index, player in enumerate(players):
                features.extend(['player'+str(10*game_index+index), 
                    'kd'+str(10*game_index+index), 'pm'+str(10*game_index+index), 
                    'kast'+str(10*game_index+index), 'adr'+str(10*game_index+index),
                    'rating'+str(10*game_index+index)])
                if player in game['player_stats']:
                    game_vec.append(player)
                    game_vec.append(game['player_stats'][player]['kd'])
                    game_vec.append(game['player_stats'][player]['pm'])
                    game_vec.append(game['player_stats'][player]['kast'].strip('%'))
                    game_vec.append(game['player_stats'][player]['adr'])
                    game_vec.append(game['player_stats'][player]['rating'])
                else:
                    game_vec.extend([np.nan]*6)

        df = pd.DataFrame([game_vec], columns=features)
        history.append(df)
    if len(history) == 0:
        return None
    else:
        return pd.concat(history)

def build_history(data):
    teams = set(data['team1'].unique())
    teams = teams.union(set(data['team2'].unique()))
    all_data = list()
    for index, team in enumerate(teams):
        print_progress_bar(index, len(teams)-1)
        history = data.loc[(data['team1'] == team)]
        history = convert_raw_history_to_feature_history(history)
        
        all_data.append(history)

        history = data.loc[(data['team2'] == team)]
        # Switch all features from team2 to team1 in order to 
        # extract the game data in the same manner as above
        opponent = history['team1'].values.copy()
        history.team1 = history.team2.values.copy()
        history.team1_players = history.team2_players.values.copy()

        # Insert overridden values
        history.team2 = opponent

        # Get matches
        history = convert_raw_history_to_feature_history(history, switch_start=True)

        all_data.append(history)

    all_data = pd.concat(all_data)

    return all_data

def load_data():
    data = pd.read_json('data/raw/hltv_result.json')
    data.sort_values(by='date', inplace=True)

    return data


def main():
    data = load_data()
    all_data = build_history(data)

    all_data.to_csv('data/interim/history_extracted_data.csv', index=False)


if __name__ == "__main__":
    main()
