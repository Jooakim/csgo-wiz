import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class HLTVResultSpider(CrawlSpider):
    name = 'hltv_spider'
    start_urls = ['https://www.hltv.org/results']

    rules = (
        Rule(LinkExtractor(restrict_xpaths='//a[@class="pagination-next"]')),
        Rule(LinkExtractor(deny='content=vod',
                           restrict_xpaths='//a[@class="a-reset"]'),
             callback='parse_match'), )

    def parse_match(self, response):
        self.logger.info('Parse match %s', response.url)
        result = GameResult()
        result['date'] = response.xpath('//div[@class="date"]/text()').extract()[0]
        match_stats = response.xpath('//div[@class="stats-content"]')
        teams = match_stats.xpath('//div[@class="teamName"]/text()').extract()
        result['team1'] = teams[0]
        result['team2'] = teams[1]

        maps = response.xpath('//div[@class=" played"]/div/div/text()').extract()
        map_scores = response.xpath('//div[@class="results"]/span/text()').extract()
        map_scores = ''.join(map_scores).split(')')

        # Extract list of class names from results
        team1_started_as = response.xpath('//div[@class="results"]/span/@class').extract()
        # Every 9th element starting from 3 has starting side
        team1_started_as = team1_started_as[3::9]

        players = match_stats.xpath('//span[@class="player-nick"]/text()').extract()

        kd = match_stats.xpath('//td[@class="kd text-center"]/text()').extract()
        kd = [i for i in kd if 'K-D' not in i]

        pm = match_stats.xpath('//td[contains(@class, "plus-minus")]/span/text()').extract()

        adr = match_stats.xpath('//td[contains(@class, "adr")]/text()').extract()
        adr = [i for i in adr if 'ADR' not in i]

        kast = match_stats.xpath('//td[contains(@class, "kast")]/text()').extract()
        kast = [i for i in kast if 'KAST' not in i]

        rating = match_stats.xpath('//td[contains(@class, "rating")]/text()').extract()
        rating = [i for i in rating if 'Rating' not in i]

        # Extract the total score for all players
        game_stats = {}
        for index, player in enumerate(players[:10]):
            stats = {
                'kd': kd[index],
                'pm': pm[index],
                'kast': kast[index],
                'rating': rating[index],
                'adr': adr[index]}
            game_stats[player] = stats
        result['team1_players'] = players[:5]
        result['team2_players'] = players[5:10]
        result['player_stats'] = game_stats

        # Extract the map individual scores
        map_results = list()
        for map_index, map_name in enumerate(maps):
            map_result = MapResult()
            map_result['map_name'] = map_name
            map_result['team1_started_as'] = team1_started_as[map_index]

            team_scores = map_scores[map_index].split()
            # half_time_scores = team_scores[1:]
            team_scores = team_scores[0].split(':')
            team1_score = int(team_scores[0])
            team2_score = int(team_scores[1])
            if team1_score > team2_score:
                map_result['winner'] = teams[0]
                map_result['winning_stats'] = team1_score
                map_result['losing_stats'] = team2_score
            else:
                map_result['winner'] = teams[1]
                map_result['winning_stats'] = team2_score
                map_result['losing_stats'] = team1_score

            map_stats = {}
            for tmp_index, player in enumerate(players[(map_index + 1) * 10:(map_index + 2) * 10]):
                offset_index = tmp_index + (map_index + 1) * 10
                stats = {
                    'kd': kd[offset_index],
                    'pm': pm[offset_index],
                    'kast': kast[offset_index],
                    'rating': rating[offset_index],
                    'adr': adr[offset_index]}
                map_stats[player] = stats
            map_result['player_stats'] = map_stats
            map_results.append(map_result)
        result['map_results'] = map_results

        yield result


class MapResult(scrapy.Item):
    map_name = scrapy.Field()
    winner = scrapy.Field()
    winning_stats = scrapy.Field()
    losing_stats = scrapy.Field()
    player_stats = scrapy.Field()
    team1_started_as = scrapy.Field()


class GameResult(scrapy.Item):
    date = scrapy.Field()
    team1 = scrapy.Field()
    team2 = scrapy.Field()
    winner = scrapy.Field()
    winning_stats = scrapy.Field()
    losing_stats = scrapy.Field()
    team1_players = scrapy.Field()
    team2_players = scrapy.Field()
    player_stats = scrapy.Field()
    map_results = scrapy.Field()
